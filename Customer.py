import random
import Exploratory

class customer (object):
    """This is a basic customer"""

    def __init__(self, customerId, budget):
        self.customerId = customerId
        self.budget = budget

    def get_probability(self, time):
        """Determine les probabilités de chaque boisson et nouriture"""

        #choisis un chiffre entre 0 et 100
        random_number = random.randint(0, 100)

        #calcule toutes les limites
        proba_frap = int(Exploratory.avg_sells(time)[0])
        proba_soda = int(proba_frap + Exploratory.avg_sells(time)[1])
        proba_coffee = int(proba_soda + Exploratory.avg_sells(time)[2])
        proba_tea = int(proba_coffee + Exploratory.avg_sells(time)[3])
        proba_water = int(proba_tea + Exploratory.avg_sells(time)[4])
        proba_milkshake = int(proba_water + Exploratory.avg_sells(time)[5])

        proba_sand = int(Exploratory.avg_sells(time)[6])
        proba_pie = int(proba_sand + Exploratory.avg_sells(time)[7])
        proba_cookie = int(proba_pie + Exploratory.avg_sells(time)[8])
        proba_muffin = int(proba_cookie + Exploratory.avg_sells(time)[9])
        proba_nothing = int(proba_muffin + Exploratory.avg_sells(time)[10])

        #determine la boisson qui sera commandé en fonction des proba
        if random_number in range(0, proba_frap):
            drink = 'frappucino'
        elif random_number in range(proba_frap, proba_soda):
            drink = 'soda'
        elif random_number in range(proba_soda, proba_coffee):
            drink = 'coffee'
        elif random_number in range(proba_tea, proba_water):
            drink = 'tea'
        elif random_number in range(proba_water, proba_milkshake):
            drink = 'water'
        else:
            drink = 'milkshake'

        # determine la nourriture qui sera commandé en fonction des proba
        if random_number in range(0, proba_sand):
            food = 'sandwich'
        elif random_number in range(proba_sand, proba_pie):
            food = 'pie'
        elif random_number in range(proba_pie, proba_cookie):
            food = 'cookie'
        elif random_number in range(proba_cookie, proba_muffin):
            food = 'muffin'
        else:
            food = 'nothing'

        return drink, food

    def calculate_price(self, drink, food, tip = 0):
        """Calculates the price of the order"""

        if drink == 'milkshake':
            price_drink = 5
        elif drink == 'frappucino':
            price_drink = 4
        elif drink == 'water':
            price_drink = 2
        else:
            price_drink = 3

        if food == 'sandwich':
            price_food = 5
        elif food == 'cookie':
            price_food = 2
        elif food == ('pie' or 'muffin'):
            price_food = 3
        else:
            price_food = 0

        price_total = price_drink + price_food + tip
        return price_total

    def get_budget(self):
        return self.budget

    def set_budget(self, drink, food):
        """Modifie le budget du client"""
        price = self.calculate_price(drink, food)
        old_budget = self.get_budget()
        new_budget = old_budget - price
        self.budget = new_budget
        return self.budget

    def take_order(self, time):
        """Enregistre la commande du client, calcule le prix, et met son budget a jour"""

        action = self.get_probability(time)
        drink = (action[0])
        food = (action[1])
        order = [drink, food]
        self.calculate_price(drink, food)
        budget = self.set_budget(drink, food)
        return order, budget

class customer_one(customer):
    """These are the customers that come only one time"""

class customer_return(customer):
    """These are the customers that return"""
    drink_orders = []
    food_orders = []


    def take_order(self, time):
        action = self.get_probability(time)
        drink = (action[0])
        food = (action[1])
        order = [drink, food]
        self.calculate_price(drink, food)
        budget = self.set_budget(drink, food)
        (customer_return.drink_orders).append(order[0])

        if food != 0:
            (customer_return.food_orders).append(order[1])
        return order, budget

class customer_one_trip(customer_one):
    """These are the customers that come once and found the place with TripAdvisor"""

    def set_budget(self, drink, food, tip = 0):
        """Modifie le budget du client"""

        price = self.calculate_price(drink, food, tip)
        old_budget = self.get_budget()
        new_budget = old_budget - price
        self.budget = new_budget
        return self.budget

    def take_order(self, time):
        tip = random.randint(0,10)
        action = self.get_probability(time)
        drink = (action[0])
        food = (action[1])
        order = [drink, food,]
        self.calculate_price(drink, food)
        budget = self.set_budget(drink, food, tip)
        return order, budget, tip

class customer_return_hipster(customer_return):
    """These are the customers that return and are hipsters"""

Jim = customer_return
