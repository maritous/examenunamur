# ExamenUnamur

### What is this repository for?
*This repository has been created to share and work on our exam project for the Data Analytics class (UNamur).
*It is divided in 3 files 

### First file: Exploratory ###
*In this file, we explore data from a dataframe containing orders of a coffeeshop :
*We first viewed briefly some data 
*Then, we made some plots with those data 
*We finally research some probabilities about the data

### Second file: Customer ###
*In this file, we created the different classes of customers 
*A customer is either a returning one or a one time customer
*Returning customer are regular one or hipsters
*One time customer are regular one or customers from Trip Advisor
*Then, we assigned the probabilities we found in the Exploratory file to give those probabilities to our customers

### Third file: Simulation ###
This file uses the two other files to make first: a simulation of one day and then: a simulation of 5 years of consumption


