#Import modules we need
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

#Opening file with data
os.chdir("C:/Users/mariu/PycharmProjects/examunamur2018/Data")
#os.chdir("/Users/oscarknoops/PycharmProjects/examunamur2018/Data")
coffee = open("Coffeebar_2013-2017.csv", "r")

#organize the csv file and delimit all data by column
df = pd.read_csv(coffee, ";")

#Seperates the TIME column in two different columns, DATE and HOUR and converts DATE in datetime format
df['HOUR'] = df['TIME'].str[-8:]
df['DATE'] = pd.to_datetime(df['TIME'].str[0:10])
df.index = df['DATE']
del df['TIME']
del df['DATE']
df = df[['HOUR', 'CUSTOMER', 'DRINKS', 'FOOD']]

#Prints the diffentt foods, drinks and the number of customers
kind_of_food = df.FOOD.unique()
kind_of_drinks = df.DRINKS.unique()
nbr_of_unique_customers = df.CUSTOMER.nunique()
#print(kind_of_food)
#print(kind_of_drinks)
#print(nbr_of_unique_customers)


#Prints the whole file
#print(df)

#Point 2

#Create the graph for food sold every year
amount_food_2013 = df['2013']['FOOD'].count()
amount_food_2014 = df['2014']['FOOD'].count()
amount_food_2015 = df['2015']['FOOD'].count()
amount_food_2016 = df['2016']['FOOD'].count()
amount_food_2017 = df['2017']['FOOD'].count()

data_food = {'YEAR': ['2013', '2014', '2015', '2016', '2017'],
             'AMOUNT FOOD': [amount_food_2013, amount_food_2014, amount_food_2015, amount_food_2016, amount_food_2017]}
food_sold_year = pd.DataFrame(data_food, columns = ['YEAR', 'AMOUNT FOOD'])
print(food_sold_year)

graph_food = food_sold_year.plot(x = 'YEAR', y = 'AMOUNT FOOD', title = 'Amount of food sold each year')
graph_food.set_xlabel("YEAR")
graph_food.set_ylabel("AMOUNT OF FOOD")
plt.show()

#Create the graph for drinks sold every year
amount_drink_2013 = df['2013']['DRINKS'].count()
amount_drink_2014 = df['2014']['DRINKS'].count()
amount_drink_2015 = df['2015']['DRINKS'].count()
amount_drink_2016 = df['2016']['DRINKS'].count()
amount_drink_2017 = df['2017']['DRINKS'].count()

data_drinks = {'YEAR': ['2013', '2014', '2015', '2016', '2017'],
             'AMOUNT DRINKS': [amount_drink_2013, amount_drink_2014, amount_drink_2015, amount_drink_2016, amount_drink_2017]}
drinks_sold_year = pd.DataFrame(data_drinks, columns = ['YEAR', 'AMOUNT DRINKS'])
#print(drinks_sold_year)

graph_drinks = drinks_sold_year.plot(x = 'YEAR', y = 'AMOUNT DRINKS', title = 'Amount of drinks sold each year')
graph_drinks.set_xlabel("YEAR")
graph_drinks.set_ylabel("AMOUNT OF DRINKS")
plt.show()

#Point 3

#Replace value 'Nan' by value 'nothing' in column 'FOOD'
df['FOOD'] = df['FOOD'].fillna(value='nothing')
#Gives the amount of food
amount_food = df['FOOD'].value_counts()
#print(amount_food)

#Gives the amount of drinks
amount_drinks = df['DRINKS'].value_counts()
#print(amount_drinks)

#Gives the total of sold food
total_food = df['FOOD'].count()
#print(total_food)

#Gives the total of sold drinks
total_drinks = df['DRINKS'].count()
#print(total_drinks)

unique_hour=df.HOUR.unique()
#Defines the different unique hours we can get

unique_hour = df.HOUR.unique()
#print(unique_hour)
#the average that a customer buys a certain food or drink at any given time
def avg_sells(hour):
    df_per_hour = df[df['HOUR']== hour]
    #Creating 2 dictionaries (one for food and one for drinks)
    f = {}
    d = {}

    #creating lists in each dictionnary
    f['sandwich'] = 0
    f['pie'] = 0
    f['muffin'] = 0
    f['cookie'] = 0
    f['nothing'] = 0
    d['frappucino'] = 0
    d['soda'] = 0
    d['coffee'] = 0
    d['tea'] = 0
    d['water'] = 0
    d['milkshake'] = 0
    #Calculating for drinks
    for i in df_per_hour['DRINKS']:
        d[i] += 1
        tot_sells_drink = d['frappucino'] + d['soda'] + d['coffee'] + d['tea'] + d['water'] + d['milkshake']
    for i in d.keys():
        #perc_drinks = round(d[i]/tot_sells_drink*100, 2)
        # avg_drinks = print('On average, the probability of a customer at %s buying %s is ' %(hour, i) , perc_drinks , ' %')

        perc_frappucino = round(d['frappucino'] / tot_sells_drink * 100, 2)
        perc_soda = round(d['soda'] / tot_sells_drink * 100, 2)
        perc_coffee = round(d['coffee'] / tot_sells_drink * 100, 2)
        perc_tea = round(d['tea'] / tot_sells_drink * 100, 2)
        perc_water = round(d['water'] / tot_sells_drink * 100, 2)
        perc_milkshake = round(d['milkshake'] / tot_sells_drink * 100, 2)
   #Calculating for food
    for j in df_per_hour['FOOD']:
            f[j] += 1
            tot_sells_food = f['sandwich'] + f['pie'] + f['muffin'] + f['cookie'] + f['nothing']
    for j in f.keys():
        #perc_food = round(f[j]/tot_sells_food*100, 2)
        # avg_food = print('On average, the probability of a customer at %s buying %s is ' %(hour, j) , perc_food ,' %')

        perc_sandwich = round(f['sandwich'] / tot_sells_food * 100, 2)
        perc_pie = round(f['pie'] / tot_sells_food * 100, 2)
        perc_muffin = round(f['muffin'] / tot_sells_food * 100, 2)
        perc_cookie = round(f['cookie'] / tot_sells_food * 100, 2)
        perc_nothing = round(f['nothing'] / tot_sells_food * 100, 2)

    return perc_frappucino, perc_soda, perc_coffee, perc_tea, perc_water, perc_milkshake, perc_sandwich, perc_pie, perc_muffin, perc_cookie, perc_nothing

#Test with an hour
#print(avg_sells('17:56:00'))

nbr_unique_hours = df.HOUR.nunique()
#print(nbr_unique_hours)