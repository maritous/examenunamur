import random
import Customer
import Exploratory
import pandas as pd
import datetime

def simulate_day():

    dico = {'TIME': [], 'ID': [], 'BUDGET': [],'DRINK': [], 'FOOD': []}
    unique_hour = Exploratory.unique_hour
    for i in unique_hour:

        random_number_1 = random.randint(1, 5)

        if random_number_1 == 1:

            customerId = 'IDR' + str(random.randint(1,1000))

            if customerId not in dico:

                random_number_2 = random.randint(1, 1000)

                if random_number_2 <= 333:
                    if customerId not in dico:
                        budget = 500
                        client = Customer.customer_return_hipster(customerId, budget)

                else:
                    if customerId not in dico:
                        budget = 250
                        client = Customer.customer_return(customerId, budget)

            else:
                if client.budget > 10:
                    time = i
                    action = client.take_order(time)
                    action
                    dico['TIME'].append(time)
                    dico['ID'].append(client.customerId)
                    dico['BUDGET'].append(client.budget)
                    dico['DRINK'].append(action[0][0])
                    dico['FOOD'].append(action[0][1])

                client_df = pd.DataFrame(data=dico, index=dico['TIME'], columns=['ID', 'BUDGET', 'DRINK', 'FOOD'])
                return client.budget

        else:
            customerId = 'IDO' + str(random.randint(100000, 999999))
            if customerId not in dico:
                random_number_3 = random.randint(1, 10)

                if random_number_3 == 1:
                    budget = 100
                    client = Customer.customer_one_trip(customerId, budget)
                else:
                    budget = 100
                    client = Customer.customer_one(customerId, budget)


        if client.budget > 10:
            time = i
            action = client.take_order(time)
            action
            dico['TIME'].append(time)
            dico['ID'].append(client.customerId)
            dico['BUDGET'].append(client.budget)
            dico['DRINK'].append(action[0][0])
            dico['FOOD'].append(action[0][1])


    client_df = pd.DataFrame(data=dico, index= dico['TIME'], columns= ['ID', 'BUDGET', 'DRINK', 'FOOD'])
    print(client_df)
    print('Next Year')

def simulate_year():


    start = datetime.datetime(2018, 1, 1).date()
    end = datetime.datetime(2022, 12, 31).date()

    for i in (start, end):
        simulate_day()

simulate_year()
simulate_day()
